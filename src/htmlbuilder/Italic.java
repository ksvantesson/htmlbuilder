package htmlbuilder;

public class Italic extends Html {
    
    private Html component;
    
    public Italic(Html component) {
        this.component = component;
    }

    @Override
    public String toHtml() {
        return "<i>" + component.toHtml() + "</i>";
    }
}
