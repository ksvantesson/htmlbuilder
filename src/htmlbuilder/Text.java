package htmlbuilder;

public class Text extends Html {

    private String text;
    
    public Text(String text) {
        this.text = text;
    }
    
    @Override
    public String toHtml() {
        return text;
    }
}
