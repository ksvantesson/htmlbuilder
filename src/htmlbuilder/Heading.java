package htmlbuilder;

public class Heading extends Html {
    
    private int level;
    private Html component;
    
    public Heading(int level, Html component) {
        this.level = level;
        this.component = component;
    }

    @Override
    public String toHtml() {
        return String.format("<h%d>%s</h%d>", level, component.toHtml(), level);
    }

}
