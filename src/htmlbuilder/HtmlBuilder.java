/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package htmlbuilder;

/**
 *
 * @author krsva1
 */
public class HtmlBuilder {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        System.out.println(new Text("hello").toHtml());
        System.out.println(new Bold(new Text("hello bold")).toHtml());
        System.out.println(new Italic(new Bold(new Text("hello italic and bold"))).toHtml());
        
        System.out.println(new Heading(1, new Bold(new Text("bold heading 1"))).toHtml());
    }
    
}
