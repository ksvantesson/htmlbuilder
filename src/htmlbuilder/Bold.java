package htmlbuilder;

public class Bold extends Html {
    private Html component;
    
    public Bold(Html component) {
        this.component = component;
    }
    
    @Override
    public String toHtml() {
        return "<b>" + component.toHtml() + "</b>";
    }
}
